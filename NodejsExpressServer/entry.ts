import expressLib = require('express');

let express = expressLib();

express.get('/', function (req, res) {
    res.send('Hello from ExpressJs!');
});

let server = express.listen(3000, function () {
    let host = server.address().address;
    let port = server.address().port;

    console.log('Example app listening at http://%s:%s', host, port);
});
