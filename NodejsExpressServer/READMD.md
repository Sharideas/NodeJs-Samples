# NODEJS EXPRESS SERVER

This readme shows how to create a server and listen a port.

---
## How-to : Create a Nodejs project

### Init project

0.  Install **NodeJs** of course.

1.  Make a empty folder.

2.  Open *Terminal* in this folder, and use command like:

        >npm init

    then the *guide* will ask you what you may want.

    Note that `nodejs project's name can no longer contain capital letters.`

3.  Use command like:

        >npm install express --save

    to install the *ExpressJs* into local folder *node_modules*.

    Use `--save` to write this into `package.json`.

4.  Now, some simple codes.


### Use TypeScript

0.  Install **TypeScript** by **NodeJs**. Use command line like:

        >npm install -g typescript
    
    I strongly suggest install **TypeScript** as a module of **NodeJs**, not only for convenience, but also to get the newest **TypeScript**.

1.  Some simple codes.

2.  You can use command-line to compile *.ts into *.js like:

        >tsc asdf.ts

    Also, you can write a config file called *tsconfig.json*, and let `tsc` use this config file.


### Use **Jetbrains WebStorm**

0.  This software is not **FREE**, buy it yourself, and I just show how to use it.

1.  *Open* your project folder.

2.  Create the `"main"` file, which declare in the `package.json`. To use **TypeScript**, set the file ex-name to `.ts`. If you chose **TypeScript**, you can use **WebStorm** to create the `tsconfig.json`.

3.  Type your code.

---
## How-To : Install node modules into seperate folder

You may already find that all the modules installed into the project, are placed at the root of folder `node_modules`, that may be terrible as the project is growing, so you can use command-line like:

    rm -rf node_modules
    npm i -g npm@2
    npm install

to solve this problem. Now you will find all the modules install into seperate folders.

---
## References
1. [ExpressJs][1.1]
2. [ExpressJs中文网][1.2]
3. [TypeScript][2.1]
4. [TypeScript - tsconfig.json][2.2]
5. [Github - TypeScript Node Starter][2.3]

---
[1.1]:http://expressjs.com/ "Express"
[1.2]:http://www.expressjs.com.cn/ "ExpressJs中文网"
[2.1]:http://www.typescriptlang.org/ "TypeScript"
[2.2]:http://www.typescriptlang.org/docs/handbook/tsconfig-json.html "TypeScript - tsconfig.json"
[2.3]:https://github.com/Microsoft/TypeScript-Node-Starter#typescript-node-starter "Github - TypeScript Node Starter"