"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var expressLib = require("express");
var express = expressLib();
express.get('/', function (req, res) {
    res.send('Hello from ExpressJs!');
});
var server = express.listen(3000, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log('Example app listening at http://%s:%s', host, port);
});
//# sourceMappingURL=entry.js.map