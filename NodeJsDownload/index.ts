import requestAsync = require('request-promise-native');
import request = require('request');
import { Readable } from 'stream';
import { Response } from 'request';

let target: string = 'http://127.0.0.1:31100/1.mp4';

// request(target, function (error, response, body) {
//     console.log('error:', error);
//     console.log('statusCode:', response && response.statusCode);
//     console.log('body:', body);
//     console.log(response.timingPhases.download);
//     console.log(response.timingPhases.total);
// });

async function test() {
    await requestAsync(target,).on('data', data).on('complete', complete);
    // await requestAsync(target).on('pipe', pipe);
}

let total : number = 0;

function data(chunk: Buffer) {
    console.log('--------->')
    console.log(chunk.byteLength);
    console.log(chunk.byteOffset);
    total += chunk.byteLength;
    console.log(total);
    console.log('<---------')
}

function complete(resp: Response, body?: string | Buffer) {
    console.log('complete :::: ');
    console.log(body.length);
    console.log(resp.headers['content-length']);
    console.log(':::: complete ');
}

test();


// import { request as r } from "http";

// r(target, (res) => {
//     res.on('data', (chunk)=>{
//             console.log(chunk.length);
//     });
// });