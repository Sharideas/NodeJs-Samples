# 示例说明

我编写这个实例代码主要参考文章都列在了最下面，本实例的代码主要也是从 **[Node.js 中文网][2]** 上面照抄下来的，所以对于代码本身这里不做过多解释，总的来说，plugin开发所需的API都比较易懂。下面主要说一下 “编译-生成”的具体步骤。

## 编译

我采用的是中规中矩的 `node-gyp` 编译命令，以及 **NW.js** 所使用的 `nw-gyp` 编译命令。前者编译的结果 *xxx.node* 可以被 **Node.js** 直接调用，后者编译的结果 *xxx.node* 则可以被 **Nw.js** 直接调用。两者编译的过程略有不同，但是在 **Windows**环境下，都需要 **Visual Studio** 支持。  
值得一提的是，最新的`node-gyp` 和 `nw-gyp` 都已经支持 **vs2015** 所附带的vc编译器了。  
另外，如果我没记错的话，编译的过程应该会用到 **Python 2.7**。  

`node-gyp`比较简单，一般安装 **Node.js** 的时候会自动安装上，如果没有安装上的话，可以使用下面的命令行来安装。

    >npm install -g node-gyp

`node-gyp`在命令行中可以像下面这样调用编译：

    >cd where_is_your_source_files
    >node-gyp configure
    ...
    ...
    ... ok
    >node-gyp build
    ...
    ...
    ... ok

两次命令都提示 `ok`就表明已经编译完成了，在目录下会生成一个 *build* 文件夹， 在文件夹中一般会有 vs 的项目文件和一个名为 *Release* 的文件夹，我们所需要的 *xxx.node* 就会在这个 *Release* 文件夹中。

---

`nw-gyp`则在编译时需要多输入几个参数。

首先还是安装，一般来说，安装了 **Node.js** 不会附带这个编译命令， 安装方法和安装 `node-gyp` 相同：

    >npm install -g nw-gyp

在本机已经安装了 `nw-gyp` 之后，可以如下调用：

    >cd where_is_your_source_files
    >nw-gyp configure --target=v0.22.3 --msvs_version=2015 --arch=x64
    ...
    ...
    ... ok
    >nw-gyp build --target=v0.22.3 --msvs_version=2015 --arch=x64
    ...
    ...
    ... ok

这里看起来就是比调用 `node-gyp` 时多了几个参数而已，还是比较简单的。唯一值得一说的是架构选择 `--arch=x64` 是生成64位二进制文件，而生成32位文件的命令是 `--arch=ia32`，貌似不是 `x86`。

## 开发环境

代码编写时会需要引用一些头文件等，这些资源基本上都可以从 [Node.js][3] 的官网上下载到。如果不使用 `node-gyp` 或 `nw-gyp` 来配置/编译的话，自己使用其他工具编译，还会需要相应的 `.lib`文件。

---

## 参考

1. [NodeJS的C++插件(Windows环境)][1]
2. [Node.js v8.4.0 文档][2]
3. [Node.js Resources][3]

---

[1]:http://www.tuicool.com/articles/bMFjMj "NodeJS的C++插件(Windows环境)"

[2]:http://nodejs.cn/api/addons.html "Node.js v8.4.0 文档"

[3]:https://nodejs.org/download/ "Node.js Resource"